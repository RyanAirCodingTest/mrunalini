﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Ryanair.Payroll.Services;
using Ryanair.Payroll.Factory;
using NSubstitute;
using Ryanair.Payroll.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ryanair.Payroll.TDD.Services
{

    /// <summary>
    /// Same TDD approach applies to other location services as well
    /// </summary>
    [TestClass]
    public class GermanyCalculatorServiceTests
    {
        private IPayrollCalculatorService _payrollService;
        private EnumLocation location;
        private IPayrollCalculatorServiceFactory _factory;

        // Set up some testing data
        [TestInitialize]
        public void Initialise()
        {
            location = EnumLocation.Germany;
            _factory = Substitute.For<IPayrollCalculatorServiceFactory>();
            var payrollService = Substitute.For<IGermanyPayrollCalculatorService>();
            IPaycheck payCheck = Substitute.For<IPaycheck>();
            payCheck.Location = "Germany";
            payCheck.Deductions = new Deductions();

            int rate = 50, hours = 10;
            double gross = rate * hours;

            double tax = gross < 400 ? gross * 25 / 100 : gross * 32 / 100;

            payCheck.Deductions.IncomeTax = tax;
            payCheck.Deductions.Pension = gross * 2 / 100;
            payCheck.GrossAmount = gross;
            payCheck.NetAmount = gross - tax - payCheck.Deductions.Pension;
            //ASSIGN
            payrollService.Calculate(rate, hours).Returns(payCheck);

            _factory.Create(location).Returns(payrollService);
           
        }
       
        [TestMethod]
        public void TestCalculate_Check_Location()
        { 

            _payrollService = _factory.Create(EnumLocation.Germany);
            var payCheck = _payrollService.Calculate(50, 10);
           
            Assert.AreEqual("Germany", payCheck.Location.ToString());


        }
        [TestMethod]
        public void TestCalculate_Check_IncomeTax()
        {

            _payrollService = _factory.Create(EnumLocation.Germany);
            var payCheck = _payrollService.Calculate(50, 10);

            Assert.AreEqual(160, payCheck.Deductions.IncomeTax);


        }
        [TestMethod]
        public void TestCalculate_Check_Pension()
        {

            _payrollService = _factory.Create(EnumLocation.Germany);
            var payCheck = _payrollService.Calculate(50, 10);

            Assert.AreEqual(10, payCheck.Deductions.Pension);


        }
        [TestMethod]
        public void TestCalculate_Check_NetAmount()
        {

            _payrollService = _factory.Create(EnumLocation.Germany);
            var payCheck = _payrollService.Calculate(50, 10);

            Assert.AreEqual(330, payCheck.NetAmount);


        }
    }
}
