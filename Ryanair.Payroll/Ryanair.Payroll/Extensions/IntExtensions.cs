﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll
{
    public static class IntExtensions
    {
        public static double GetPercentageValue(this double value, double percent)
        {
            return value * percent / 100;
        }
    }
}
