﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NLog;
using Ryanair.Payroll.Dependency.Container;
using Ryanair.Payroll.Factory;
using Ryanair.Payroll.Models;
using Ryanair.Payroll.Services;
using Component = Castle.MicroKernel.Registration.Component;

namespace Ryanair.Payroll.Dependency
{
    public class DependencyInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
            var applicationContainer = new HostContainer(container);
            container.Register(
                Component.For<IContainer>().Instance(applicationContainer),
                Component.For<ILogger>().Instance(LogManager.GetCurrentClassLogger()),
                Component.For<IDeductions>().Instance(new Deductions()),
                Component.For<IPaycheck>().Instance(new Paycheck()),
                Component.For<IEmployee>().Instance(new Employee())
                );
            container.Register(
                Component.For<IGermanyPayrollCalculatorService>().Instance(new GermanyPayrollCalculatorService()),
                Component.For<IItalyPayrollCalculatorService>().Instance(new ItalyPayrollCalculatorService()),
                Component.For<IIrelandPayrollCalculatorService>().Instance(new IrelandPayrollCalculatorService())
               
                );
            container.Register(
            Component.For<IPayrollCalculatorServiceFactory>().Instance(new PayrollCalculatorServiceFactory(container.Resolve<IContainer>())
            ));
        }
    }
}
