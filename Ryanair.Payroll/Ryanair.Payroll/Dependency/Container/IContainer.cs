﻿using System;
using System.Collections;

namespace Ryanair.Payroll.Dependency.Container
{
    public interface IContainer: IDisposable
    {
        object Resolve(Type type);
        T Resolve<T>();
        T Resolve<T>(IDictionary args);
        void Release(object instance);
    }
}