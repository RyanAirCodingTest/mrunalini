﻿using System;
using System.Collections;
using Castle.Windsor;

namespace Ryanair.Payroll.Dependency.Container
{
    internal class HostContainer : IContainer
    {
        private IWindsorContainer _instance;
        private bool _disposed;

        public HostContainer(IWindsorContainer containerInstance)
        {
            _instance = containerInstance;
        }

        public object Resolve(Type type)
        {
            return _instance.Resolve(type);
        }

        public T Resolve<T>()
        {
            return _instance.Resolve<T>();
        }

        public T Resolve<T>(IDictionary args)
        {
            return _instance.Resolve<T>(args);
        }

        public void Release(object instance)
        {
            _instance.Release(instance);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _instance.Dispose();
                    _instance = null;
                }
            }
            _disposed = true;
        }
        
    }
}
