﻿using Castle.Windsor;
using Ryanair.Payroll.Dependency.Container;

namespace Ryanair.Payroll.Dependency
{
    internal class DependencyManager
    {
        private static IContainer _instance;
        private static readonly object Lock = new object();

        private static IContainer Create()
        {
            var container = new WindsorContainer();
            container.Install(new DependencyInstaller());
            

            return container.Resolve<IContainer>();
        }

        public static IContainer Register()
        {
            lock (Lock)
            {
                if (null != _instance) return _instance;

                _instance = Create();
                return _instance;
            }
        }
    }
}
