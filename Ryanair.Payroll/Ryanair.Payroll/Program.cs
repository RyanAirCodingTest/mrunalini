﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ryanair.Payroll.Dependency;
using Ryanair.Payroll.Dependency.Container;
using Ryanair.Payroll.Models;
using Ryanair.Payroll.Factory;
using Ryanair.Payroll.Services;
using NLog;

namespace Ryanair.Payroll
{
    public class Program
    {
        
        //Please READ
        //I have not handled any exception
        private static IContainer _container;
        private static IEmployee _employee;
        private static IPayrollCalculatorServiceFactory _factory;
        private static IPayrollCalculatorService _payrollService;
        private static ILogger _logger;
        static void Main(string[] args)
        {
            NlogConfig.Setup();
          _container=  DependencyManager.Register();
            _logger = _container.Resolve<ILogger>();
           _employee= _container.Resolve<IEmployee>();
            _factory=_container.Resolve<IPayrollCalculatorServiceFactory>();
            _logger.Info("Program started...");
            A:
            Console.Write("Please enter the hours worked: ");
           _employee.HoursWorked= Convert.ToInt32(Console.ReadLine());
            Console.Write("\n Please enter the hourly rate: ");
            _employee.HourlyRate = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n Please enter the employee’s location(Germany/Italy/Ireland): ");
            _employee.Location = Console.ReadLine();
            
            
            EnumLocation location;
            Enum.TryParse(_employee.Location, out location);
            _payrollService = _factory.Create(location); 
            var payCheck = _payrollService.Calculate(_employee.HourlyRate, _employee.HoursWorked);
            StringBuilder output = new StringBuilder();
            output.AppendLine("Employee location :" + _employee.Location);
            output.AppendLine("Gross Amount: £" + payCheck.GrossAmount);
            output.AppendLine("Less deductions");
            output.AppendLine("------------------------");
            output.AppendLine("Income Tax : £" + payCheck.Deductions.IncomeTax);
            output.AppendLine("Universal Social Charge: £" + payCheck.Deductions.UniversalSurCharge);
            output.AppendLine("Pension: £" + payCheck.Deductions.Pension);
            output.AppendLine("=========================");
           output.AppendLine("Net Amount: £" + payCheck.NetAmount);
            output.AppendLine("=========================");
            Console.Write(output.ToString());

            goto A;
                        Console.Read();
        }
    }
}
