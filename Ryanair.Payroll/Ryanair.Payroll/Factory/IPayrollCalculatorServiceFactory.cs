﻿using Ryanair.Payroll.Dependency.Container;
using Ryanair.Payroll.Models;
using Ryanair.Payroll.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll.Factory
{
    public interface IPayrollCalculatorServiceFactory
    {
        IPayrollCalculatorService Create(EnumLocation location);
    }
    public class PayrollCalculatorServiceFactory : IPayrollCalculatorServiceFactory
    {

        private IDictionary<EnumLocation, IPayrollCalculatorService> _servicesBag;
        public PayrollCalculatorServiceFactory(IContainer container)
        {
            _servicesBag= new Dictionary<EnumLocation, IPayrollCalculatorService>();
            _servicesBag.Add(EnumLocation.Germany,  container.Resolve<IGermanyPayrollCalculatorService>());
            _servicesBag.Add(EnumLocation.Ireland, container.Resolve<IIrelandPayrollCalculatorService>());
            _servicesBag.Add(EnumLocation.Italy, container.Resolve<IItalyPayrollCalculatorService>());
        }

        public IPayrollCalculatorService Create(EnumLocation location)
        {
            return _servicesBag[location];
        }
    }
}
