﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll.Models
{
    public interface IPaycheck
    {
         string Location { get; set; }
         double GrossAmount { get; set; }
         double NetAmount { get; set; }

         IDeductions Deductions { get; set; }
    }
}
