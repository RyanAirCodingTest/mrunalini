﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll.Models
{
    public interface IEmployee
    {
        string Location { get; set; }

         int HourlyRate { get; set; }
        int HoursWorked { get; set; }

    }
}
