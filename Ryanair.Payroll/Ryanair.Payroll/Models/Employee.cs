﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll.Models
{
    public class Employee : IEmployee
    {
        public string Location { get; set; }

        public int HourlyRate { get; set; }
        public int HoursWorked { get; set; }

    }
}
