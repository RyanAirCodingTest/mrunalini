﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll.Models
{
    public class Paycheck : IPaycheck
    {
        public string Location { get; set; }
        public double GrossAmount { get; set; }
        public double NetAmount { get; set; }

        public IDeductions Deductions { get; set; }
    }
}
