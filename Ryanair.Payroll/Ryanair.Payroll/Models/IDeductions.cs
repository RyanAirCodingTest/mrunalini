﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll.Models
{
    public interface IDeductions
    {
        double IncomeTax { get; set; }
        double UniversalSurCharge { get; set; }
        double Pension { get; set; }
    }
}
