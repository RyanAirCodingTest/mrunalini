﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll.Models
{
    public class Deductions : IDeductions
    {
        public double IncomeTax { get; set; }
        public double UniversalSurCharge { get; set; }
        public double Pension { get; set; }
    }
}
