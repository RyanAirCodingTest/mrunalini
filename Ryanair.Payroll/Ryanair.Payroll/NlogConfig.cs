﻿using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll
{
    public static class NlogConfig
    {
        public static void Setup()
        {

            LoggingConfiguration config = new LoggingConfiguration();

            // Step 2. Create targets and add them to the configuration

         
            FileTarget fileTarget = new FileTarget();
            config.AddTarget("file", fileTarget);

            // Step 3. Set target properties

           
            fileTarget.FileName = "${basedir}/log.txt";
            fileTarget.Layout = "${date:format=HH\\:MM\\:ss} ${logger} ${message}";

            // Step 4. Define rules

         
            LoggingRule rule2 = new LoggingRule("*", LogLevel.Debug, fileTarget);
            config.LoggingRules.Add(rule2);

            // Step 5. Activate the configuration

            LogManager.Configuration = config;
        }
    }
}
