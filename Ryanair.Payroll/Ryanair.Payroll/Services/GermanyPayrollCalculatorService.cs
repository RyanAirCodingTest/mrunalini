﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ryanair.Payroll.Models;

namespace Ryanair.Payroll.Services
{
    public class GermanyPayrollCalculatorService : IGermanyPayrollCalculatorService
    {
        private const double taxPercentBelow400 = 25;
        private const double taxPercentAbove400 = 32;
        private const double pensionPercent = 2;
        public IPaycheck Calculate(int rate, int hours)
        {
            var payCheck = new Paycheck();
            payCheck.Deductions = new Deductions();

            double gross = rate * hours;

            double tax = gross < 400?gross.GetPercentageValue(taxPercentBelow400): gross.GetPercentageValue(taxPercentAbove400); 

            payCheck.Deductions.IncomeTax = tax;
            payCheck.Deductions.Pension = gross.GetPercentageValue(pensionPercent);
            payCheck.GrossAmount = gross;
            payCheck.NetAmount = gross - tax - payCheck.Deductions.Pension;

            return payCheck;
        }
    }
    public interface IGermanyPayrollCalculatorService : IPayrollCalculatorService
    {

    }
}
