﻿using Ryanair.Payroll.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryanair.Payroll.Services
{
    public interface IPayrollCalculatorService
    {
        IPaycheck Calculate(int rate, int hours);
    }
}
