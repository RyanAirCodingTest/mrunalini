﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ryanair.Payroll.Models;

namespace Ryanair.Payroll.Services
{
    public class IrelandPayrollCalculatorService : IIrelandPayrollCalculatorService
    {
        private const double taxPercentBelow600 = 25;
        private const double taxPercentAbove600 = 40;
        private const double universalSurchargePercentBelow500 = 7;
        private const double universalSurchargePercentAbove500 = 8;
        private const double pensionPercent = 4;
        public IPaycheck Calculate(int rate, int hours)
        {
            var payCheck = new Paycheck();
            payCheck.Deductions = new Deductions();

            double gross = rate * hours;

            
            payCheck.Deductions.IncomeTax = gross < 600 ? gross.GetPercentageValue(taxPercentBelow600) : gross.GetPercentageValue(taxPercentAbove600);

            payCheck.Deductions.UniversalSurCharge = gross < 500 ? gross.GetPercentageValue(universalSurchargePercentBelow500) : gross.GetPercentageValue(universalSurchargePercentAbove500);
            payCheck.Deductions.Pension = gross.GetPercentageValue(pensionPercent);
            payCheck.GrossAmount = gross;
            payCheck.NetAmount = gross - payCheck.Deductions.IncomeTax - payCheck.Deductions.Pension- payCheck.Deductions.UniversalSurCharge;

            return payCheck;
        }
    }
    public interface IIrelandPayrollCalculatorService : IPayrollCalculatorService
    {

    }
}
