﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ryanair.Payroll.Models;

namespace Ryanair.Payroll.Services
{
    public class ItalyPayrollCalculatorService : IItalyPayrollCalculatorService
    {
        private const double taxPercentFlat = 25;
      
        private const double pensionPercent = 9.19;
        public IPaycheck Calculate(int rate, int hours)
        {
            var payCheck = new Paycheck();
            payCheck.Deductions = new Deductions();

            double gross = rate * hours;


            payCheck.Deductions.IncomeTax = gross.GetPercentageValue(taxPercentFlat);

            payCheck.Deductions.Pension = gross.GetPercentageValue(pensionPercent);
            payCheck.GrossAmount = gross;
            payCheck.NetAmount = gross - payCheck.Deductions.IncomeTax - payCheck.Deductions.Pension - payCheck.Deductions.UniversalSurCharge;

            return payCheck;
        }
    }
    public interface IItalyPayrollCalculatorService : IPayrollCalculatorService
    { }
}
